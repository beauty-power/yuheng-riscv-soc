`timescale 1ns / 1ps
/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-29     Lyons        first version
 */

module core_tb(
    );

`include "../pa_chip_param.v"

reg                             sys_clk;
reg                             sys_rst_n;

wire                            TXD;

pa_chip_top u_pa_chip_top (
    .clk_i                      (sys_clk),
    .rst_n_i                    (sys_rst_n),

    .rxd                        (),
    .txd                        (TXD)
);

core_uart_monitor_tb u_core_uart_monitor_tb (
    .clk_i                      (sys_clk),
    .rst_n_i                    (sys_rst_n),

    .rxd                        (TXD),
    .txd                        ()
);

initial begin
    sys_clk = 1;
    sys_rst_n = 0;

`ifndef ROM_MODE_BRAM
    $readmemh("../../../../../tb/inst_rom.data", u_pa_chip_top.u_pa_core_top.u_pa_core_ifu.u_pa_core_itcm._rom);
`endif
end

always begin
    @ (posedge sys_clk) sys_rst_n = 0;
    @ (posedge sys_clk) sys_rst_n = 1;

    while (1) begin
        @ (posedge sys_clk);
    end

    $stop();
end

always #((32'd1_000_000_000/`CPU_FREQ_HZ)/2) sys_clk = ~sys_clk;

endmodule